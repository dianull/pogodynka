package pl.happydroid.pogodynka.utils;

public class Constants {

    public static final String OWM_API_ADDRESS = "http://api.openweathermap.org/data/2.5/weather";
    public static final String API_ID = "541133a6dd7623b8cb5004acc2aededa";

    public static final double KALVIN_CONVERSION_VAL = 273.15;
    public static final String PRESURE_SUFFIX = "hPa";
    public static final String WIND_SUFFIX = "m/s";

    public static final int DEFAULT_MAP_ZOOM = 3;

    public static final String OWM_LAYER_TEMP = "temp_new";
    public static final String OWM_LAYER_WIND = "wind_new";
    public static final String OWM_LAYER_RAIN = "precipitation_new";
    public static final String OWM_LAYER_PRESSURE = "pressure_new";
    public static final String OWM_LAYER_SOURCE_URL = "https://tile.openweathermap.org/map/";

    public static final String WEATHER_DESC_CLEAR_SKY = "clear sky";
    public static final String WEATHER_DESC_FEW_CLOUDS = "few couds";
    public static final String WEATHER_DESC_SCATTERED_CLOUDS = "scattered clouds";
    public static final String WEATHER_DESC_BROKEN_CLOUDS = "broken clouds";
    public static final String WEATHER_DESC_SHOWER_RAIN = "shower rain";
    public static final String WEATHER_DESC_RAIN = "rain";
    public static final String WEATHER_DESC_THUNDERSTORM = "thunderstorm";
    public static final String WEATHER_DESC_SNOW = "snow";
    public static final String WEATHER_DESC_MIST = "mist";

    public static final String PREFS_SHOW_TOOLTIP = "showToolTipOnce";


}
