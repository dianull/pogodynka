package pl.happydroid.pogodynka.location;

import android.app.Activity;
import android.location.Location;

public interface LocationReceiver {

    Activity getReceivingActivity();

    void onNewLocationReceived();

}

