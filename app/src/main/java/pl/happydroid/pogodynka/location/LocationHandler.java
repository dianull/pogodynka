package pl.happydroid.pogodynka.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import pl.happydroid.pogodynka.MainActivity;
import pl.happydroid.pogodynka.R;

import static pl.happydroid.pogodynka.MainActivity.REQUEST_ENABLE_LOCATION;

public class LocationHandler implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String TAG = "LocationHandler";
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int UPDATE_INTERVAL_IN_SECONDS = 700;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;

    private LocationReceiver receiver;
    private GoogleApiClient locationClient;
    private LocationRequest locationRequest;
    private Location currentLocation;
    private AlertDialog.Builder dialog;
    private Context context;


    public LocationHandler(Context context, LocationReceiver receiver) {

        this.locationClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        this.receiver = receiver;
        this.context = context;

        this.locationRequest = LocationRequest.create();
        this.locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        this.locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

    }

    public void checkLocationEnabled() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(locationClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        initUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(receiver.getReceivingActivity(), REQUEST_ENABLE_LOCATION);
                        } catch (IntentSender.SendIntentException e) { }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) { }


    @Override
    public void onConnected(Bundle connectionHint) { }

    private void initUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(locationClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) { }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        if (receiver != null) {
            receiver.onNewLocationReceived();
        }
    }


    public void connect() {
        if (googlePlayServicesAvailable() && !locationClient.isConnected()) {
            locationClient.connect();
        }
    }


    public void disconnect() {
        if (locationClient.isConnected() && googlePlayServicesAvailable()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(locationClient, this);
            locationClient.disconnect();
        }
    }

    public Location getCurrentLocation() {
        if (googlePlayServicesAvailable()) {
            if (currentLocation == null) {
                if (locationClient.isConnected()) {

                    currentLocation = LocationServices.FusedLocationApi.getLastLocation(locationClient);
                } else if (!locationClient.isConnecting()) {
                    locationClient.connect();
                }
            }
        }
        if (currentLocation == null) {
            Activity activity = receiver.getReceivingActivity();
            Toast.makeText(activity, activity.getResources().getString(R.string.no_location), Toast.LENGTH_SHORT).show();
        }
        return currentLocation;
    }

    public LatLng getCurrentLatLng() {
        Location loc = getCurrentLocation();

        LatLng latLng = null;

        if (loc != null) {
            return new LatLng(loc.getLatitude(), loc.getLongitude());
        }

        return latLng;
    }


    private boolean googlePlayServicesAvailable() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(receiver.getReceivingActivity()) == ConnectionResult.SUCCESS;
    }

}
