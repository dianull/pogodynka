package pl.happydroid.pogodynka;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.style.layers.RasterLayer;
import com.mapbox.mapboxsdk.style.sources.RasterSource;
import com.mapbox.mapboxsdk.style.sources.TileSet;
import com.spyhunter99.supertooltips.ToolTip;
import com.spyhunter99.supertooltips.ToolTipManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.happydroid.pogodynka.model.WeatherData;
import pl.happydroid.pogodynka.presenter.WeatherPresenter;

import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.rasterOpacity;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;
import static pl.happydroid.pogodynka.utils.Constants.API_ID;
import static pl.happydroid.pogodynka.utils.Constants.DEFAULT_MAP_ZOOM;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_PRESSURE;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_RAIN;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_SOURCE_URL;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_TEMP;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_WIND;
import static pl.happydroid.pogodynka.utils.Constants.PREFS_SHOW_TOOLTIP;
import static pl.happydroid.pogodynka.utils.Constants.PRESURE_SUFFIX;
import static pl.happydroid.pogodynka.utils.Constants.WIND_SUFFIX;

public class MainActivity extends AppCompatActivity implements WeatherPresenter.UIListener {


    public static final int PERMISSIONS_REQUEST_READ_FINE_LOCATION = 1000;
    public static final int REQUEST_ENABLE_LOCATION = 100;

    private MapView mapView;
    private MapboxMap map;
    private WeatherPresenter presenter;
    private View toolbarView;
    private String activeLayer = OWM_LAYER_TEMP;
    private Marker positionMarker = null;

    @BindView(R.id.temp_layout)
    LinearLayout tempLayout;

    @BindView(R.id.humidity_layout)
    LinearLayout humidityLayout;

    @BindView(R.id.wind_layout)
    LinearLayout windLayout;

    @BindView(R.id.pressure_layout)
    LinearLayout pressureLayout;

    @BindView(R.id.info_layout)
    LinearLayout infoLayout;

    @BindView(R.id.progress_bar)
    RelativeLayout progressBar;

    @BindView(R.id.city)
    TextView cityTV;

    @BindView(R.id.refresh)
    ImageView refreshImg;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(getApplicationContext(), getString(R.string.mapbox_access_token));

        setContentView(R.layout.activity_main);

        setupActionBar();

        ButterKnife.bind(this);

        showHideWeatherInfo(false);

        setupMapView(savedInstanceState);

        presenter = new WeatherPresenter(this, this);

        setupUIListeners();

    }

    private void setupActionBar() {
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        toolbarView = inflator.inflate(R.layout.weather_info, null);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(toolbarView);
        getSupportActionBar().setCustomView(toolbarView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        Toolbar toolbar = (Toolbar)toolbarView.getParent();
        toolbar.setContentInsetsAbsolute(0,0);

    }

    public void showToolTipOnce() {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (!sharedPreferences.getBoolean(PREFS_SHOW_TOOLTIP, false)) {

            SharedPreferences.Editor sharedPreferencesEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            sharedPreferencesEditor.putBoolean(PREFS_SHOW_TOOLTIP, true);
            sharedPreferencesEditor.apply();

            ToolTipManager tooltips = new ToolTipManager(this);

            ToolTip toolTip = new ToolTip().withPosition(ToolTip.Position.LEFT)
                    .withText(R.string.tooltip)
                    .withColor(getResources().getColor(R.color.colorGradientEnd))
                    .withAnimationType(ToolTip.AnimationType.FROM_MASTER_VIEW)
                    .withShadow();
            tooltips.showToolTip(toolTip, pressureLayout);
        }
    }

    private void setupUIListeners() {
        refreshImg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean returnValue = true;
                if (event.getAction()== MotionEvent.ACTION_UP){
                    returnValue = false;
                    presenter.getWeatherData();
                }
                return returnValue;
            }
        });

        tempLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean returnValue = true;
                if (event.getAction()== MotionEvent.ACTION_UP){
                    returnValue = false;
                    refreshLayers(OWM_LAYER_TEMP);
                }
                return returnValue;
            }
        });

        humidityLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean returnValue = true;
                if (event.getAction()== MotionEvent.ACTION_UP){
                    returnValue = false;
                    refreshLayers(OWM_LAYER_RAIN);
                }
                return returnValue;
            }
        });

        windLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean returnValue = true;
                if (event.getAction()== MotionEvent.ACTION_UP){
                    returnValue = false;
                    refreshLayers(OWM_LAYER_WIND);
                }
                return returnValue;
            }
        });

        pressureLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean returnValue = true;
                if (event.getAction()== MotionEvent.ACTION_UP){
                    returnValue = false;
                    refreshLayers(OWM_LAYER_PRESSURE);
                }
                return returnValue;
            }
        });
    }

    public void showHideWeatherInfo(boolean show) {
        cityTV.setVisibility(show ? View.VISIBLE : View.GONE);
        infoLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        refreshImg.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void showHideProgressBar(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void refreshLayers(String newLayer) {
        map.getLayer(activeLayer + "_layer").setProperties(visibility(NONE));
        activeLayer = newLayer;
        map.getLayer(activeLayer + "_layer").setProperties(visibility(VISIBLE));
    }

    private void setupMapView(Bundle savedInstanceState) {
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                map = mapboxMap;

                for (String sourceName : WeatherData.getLayerSources()) {
                    RasterSource rs = new RasterSource(sourceName + "_source", new TileSet("tileset", getUrlSourceBylayer(sourceName)));
                    map.addSource(rs);
                    RasterLayer layer = new RasterLayer(sourceName + "_layer", sourceName + "_source");
                    layer.setProperties(visibility(NONE));
                    map.addLayer(layer);
                }
                map.getLayer(activeLayer + "_layer").setProperties(visibility(VISIBLE));

                if (presenter.getCurrentLocation() != null)
                    drawMapMarker(presenter.getCurrentLocation());
            }
        });

    }

    public void drawMapMarker(com.google.android.gms.maps.model.LatLng currentLatLng) {
        if (map != null) {
            if (positionMarker == null) {
                positionMarker = map.addMarker(new MarkerOptions().position(new LatLng(currentLatLng.latitude, currentLatLng.longitude)));
            } else {
                positionMarker.setPosition(new LatLng(currentLatLng.latitude, currentLatLng.longitude));
            }
        }
    }

    private String getUrlSourceBylayer(String layer) {
        return OWM_LAYER_SOURCE_URL + layer + "/{z}/{x}/{y}.png?appid=" + API_ID;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_LOCATION) {
            String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (provider.equals("")) {
                showAlertMessage(getResources().getString(R.string.location_disabled));
            }

        }
    }


    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission( getApplicationContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                showAlertMessage(getResources().getString(R.string.permissions_disabled_rationale));

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_REQUEST_READ_FINE_LOCATION);
            }
        } else {
            presenter.checkLocationEnabled();
        }
    }


    private void showAlertMessage(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(R.string.warning);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.checkLocationEnabled();
                } else {
                    showAlertMessage(getResources().getString(R.string.permissions_disabled_rationale));
                }
                return;
            }
        }
    }

    public void setCameraPosition(Location camerpostion) {
        if (map != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(camerpostion.getLatitude(),
                    camerpostion.getLongitude()), DEFAULT_MAP_ZOOM));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        presenter.connect();
        checkPermissions();
    }


    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.disconnect();
        mapView.onPause();
     }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        mapView.onSaveInstanceState(bundle);
    }


    @Override
    public void updateWeatherUI(WeatherData weatherData, Drawable iconSourceByWeather) {

        if (weatherData == null) {
            showAlertMessage(getResources().getString(R.string.no_weather_data));
        } else {
            String country = weatherData.getSys().getCountry();
            country = country.isEmpty() ? "" : ", " + country;
            TextView cityTV = toolbarView.findViewById(R.id.city);
            cityTV.setText(weatherData.getCity() + country);
            TextView tempTV = toolbarView.findViewById(R.id.temp);
            tempTV.setText(String.valueOf(Math.round(weatherData.getMain().getTemp())) + "°");
            TextView windTV = toolbarView.findViewById(R.id.wind);
            windTV.setText(String.valueOf(weatherData.getWind().getSpeed()) + " " + WIND_SUFFIX);
            TextView humidityTV = toolbarView.findViewById(R.id.humidity);
            humidityTV.setText(String.valueOf(weatherData.getMain().getHumidity()) + "%");
            TextView pressureTV = toolbarView.findViewById(R.id.pressure);
            pressureTV.setText(String.valueOf(weatherData.getMain().getPressure()) + " " + PRESURE_SUFFIX);

            ImageView icon = toolbarView.findViewById(R.id.temp_icon);
            icon.setImageDrawable(iconSourceByWeather);
        }
    }

}
