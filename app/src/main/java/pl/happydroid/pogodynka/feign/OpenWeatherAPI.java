package pl.happydroid.pogodynka.feign;

import com.netflix.hystrix.HystrixCommand;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

@Headers({"Accept: application/json;charset=utf-8", "Content-Type: application/json;charset=utf-8"})
public interface OpenWeatherAPI {

    @RequestLine("GET /?lat={lat}&lon={lon}&APPID={id}")
    public HystrixCommand<String> getWeatherData(@Param("lat") String lat, @Param("lon") String lon, @Param("id") String id);
}
