package pl.happydroid.pogodynka.feign;

import java.util.concurrent.Future;

import feign.hystrix.HystrixFeign;
import pl.happydroid.pogodynka.utils.Constants;

import static pl.happydroid.pogodynka.utils.Constants.API_ID;

public class OpenWeatherService {

    public Future<String> getWeatherData(String lat, String lon) {

       OpenWeatherAPI settingsAPI = HystrixFeign.builder().target(OpenWeatherAPI.class, Constants.OWM_API_ADDRESS);

       return settingsAPI.getWeatherData(lat, lon, API_ID).queue();
    }
}
