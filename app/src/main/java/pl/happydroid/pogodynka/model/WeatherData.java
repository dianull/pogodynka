package pl.happydroid.pogodynka.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_PRESSURE;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_RAIN;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_TEMP;
import static pl.happydroid.pogodynka.utils.Constants.OWM_LAYER_WIND;

public class WeatherData {

    @SerializedName("coord")
    @Expose
    private Coord coord;
    @SerializedName("weather")
    @Expose
    private List<Weather> weather = null;
    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("main")
    @Expose
    private Main main;
    @SerializedName("visibility")
    @Expose
    private Integer visibility;
    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("clouds")
    @Expose
    private Clouds clouds;
    @SerializedName("sys")
    @Expose
    private Sys sys;

    private int icon;
    private String city = "";

    static String[] sources = {OWM_LAYER_TEMP, OWM_LAYER_WIND, OWM_LAYER_RAIN, OWM_LAYER_PRESSURE};

    public WeatherData() {
    }

    public static String[] getLayerSources() {
        return sources;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }
}
