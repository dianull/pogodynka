package pl.happydroid.pogodynka.presenter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import pl.happydroid.pogodynka.MainActivity;
import pl.happydroid.pogodynka.feign.OpenWeatherService;
import pl.happydroid.pogodynka.R;
import pl.happydroid.pogodynka.location.LocationHandler;
import pl.happydroid.pogodynka.location.LocationReceiver;
import pl.happydroid.pogodynka.model.Weather;
import pl.happydroid.pogodynka.model.WeatherData;

import static pl.happydroid.pogodynka.utils.Constants.KALVIN_CONVERSION_VAL;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_BROKEN_CLOUDS;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_CLEAR_SKY;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_FEW_CLOUDS;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_MIST;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_RAIN;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_SCATTERED_CLOUDS;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_SHOWER_RAIN;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_SNOW;
import static pl.happydroid.pogodynka.utils.Constants.WEATHER_DESC_THUNDERSTORM;

public class WeatherPresenter<T extends WeatherPresenter.LocationViewModel>  implements LocationReceiver {

    private final LocationHandler locationHandler;

    private UIListener uiListener;

    private WeatherData weatherData = null;

    public interface UIListener {
        void updateWeatherUI(WeatherData weatherData, Drawable iconSourceByWeather);
        void setCameraPosition(Location location);
        void showHideProgressBar(boolean show);
        void showHideWeatherInfo(boolean show);
        void drawMapMarker(LatLng currentLatLng);
        void showToolTipOnce();
    }


    public WeatherPresenter(Context context, MainActivity listener) {
        this.uiListener = listener;
        this.locationHandler = new LocationHandler(context, this);

    }

    public LatLng getCurrentLocation() {
        return locationHandler.getCurrentLatLng();
    }

    public void checkLocationEnabled() {
        locationHandler.checkLocationEnabled();

    }
    public void checkCurrentCity(Context context) {
        LatLng currentLocation = locationHandler.getCurrentLatLng();
        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(currentLocation.latitude, currentLocation.longitude, 1);
            if (addresses.size() > 0) {
                String cityName = addresses.get(0).getLocality();
                weatherData.setCity(cityName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Activity getReceivingActivity() {
        return (Activity) uiListener;
    }


    @Override
    public void onNewLocationReceived() {
        if (weatherData == null)
            weatherData = new WeatherData();

        uiListener.drawMapMarker(locationHandler.getCurrentLatLng());
        getWeatherData();
    }

    public interface LocationViewModel {
        Activity getActivity();
    }

    public void connect() {
        locationHandler.connect();
    }

    public void disconnect() {
        locationHandler.disconnect();
    }

    public void getWeatherData() {
        new getWeatherData().execute("");
    }

    private class getWeatherData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            uiListener.showHideProgressBar(true);
            uiListener.showHideWeatherInfo(false);
        }

        @Override
        protected String doInBackground(String... params) {
            Future<String> raw = new OpenWeatherService().getWeatherData(String.valueOf(locationHandler.getCurrentLatLng().latitude),
                    String.valueOf(locationHandler.getCurrentLatLng().longitude));
            String result = "";

            try {
                result = raw.get(2, TimeUnit.MINUTES);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            uiListener.showHideProgressBar(false);
            uiListener.showHideWeatherInfo(true);
            uiListener.showToolTipOnce();

            parseWeatherData(str);
        }
    }

    private void parseWeatherData(String str) {
        if (str != null && str.isEmpty()) {
            uiListener.updateWeatherUI(null, null);
        } else {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            weatherData = gson.fromJson(str, WeatherData.class);
            if (weatherData == null) {
                uiListener.updateWeatherUI(weatherData, null);
            } else {
                checkCurrentCity((Context) uiListener);
                weatherData.getMain().setTemp(convertKalvinsToCelcius(weatherData.getMain().getTemp()));

                uiListener.updateWeatherUI(weatherData, getIconSourceByWeather());
                uiListener.setCameraPosition(locationHandler.getCurrentLocation());
            }
        }
    }


    private Drawable getIconSourceByWeather() {

        Drawable icon = null;
        Weather weather = weatherData.getWeather().get(0);

        if (weather != null) {
            boolean isNight = weather.getIcon().endsWith("n");
            switch (weather.getDescription()) {
                case WEATHER_DESC_CLEAR_SKY:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_moon_and_stars)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_sun);
                    break;
                case WEATHER_DESC_FEW_CLOUDS:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_partly_cloudy_day);
                    break;
                case WEATHER_DESC_SCATTERED_CLOUDS:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_cloud);
                    break;
                case WEATHER_DESC_BROKEN_CLOUDS:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_cloud);
                    break;
                case WEATHER_DESC_SHOWER_RAIN:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_rain);
                    break;
                case WEATHER_DESC_RAIN:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_rain);
                    break;
                case WEATHER_DESC_THUNDERSTORM:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_stormy_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_storm);
                    break;
                case WEATHER_DESC_SNOW:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_snowy_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_snow);
                    break;
                case WEATHER_DESC_MIST:
                    icon = isNight ? ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_night)
                            : ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_haze);
                    break;
                default:
                    icon = ((Activity) uiListener).getResources().getDrawable(R.drawable.icons8_partly_cloudy_day);
                    break;
            }

        }
        return icon;
    }

    private Double convertKalvinsToCelcius(Double kalvins) {
        return (kalvins - KALVIN_CONVERSION_VAL);
    }
}
